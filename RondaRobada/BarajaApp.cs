﻿using System;
using System.Collections.Generic;

namespace RondaRobada
{
    public class Carta
    {
        public int Numero { get; set; }
        public string Palo { get; set; }

        public Carta(int numero, string palo)
        {
            this.Numero = numero;
            this.Palo = palo;
        }
    }

    public class Baraja
    {
        private List<Carta> barajaCartas;
        private List<Carta> montonCartas;

        public Baraja()
        {
            barajaCartas = new List<Carta>();
            montonCartas = new List<Carta>();

            // Creamos las 40 cartas de la baraja española
            string[] palos = { "Espadas", "Bastos", "Oros", "Copas" };
            for (int i = 1; i <= 12; i++)
            {
                if (i != 8 && i != 9)
                {
                    foreach (string palo in palos)
                    {
                        barajaCartas.Add(new Carta(i, palo));
                    }
                }
            }
        }

        public void Barajar()
        {
            Random rnd = new Random();
            int totalCartas = barajaCartas.Count;
            while (totalCartas > 1)
            {
                totalCartas--;
                int  i = rnd.Next(totalCartas + 1);
                Carta carta = barajaCartas[i];
                barajaCartas[i] = barajaCartas[totalCartas];
                barajaCartas[totalCartas] = carta;
            }
        }

        public Carta SiguienteCarta()
        {
            if (barajaCartas.Count == 0)
            {
                Console.WriteLine("No hay más cartas en la baraja");
                return null;
            }

            Carta carta = barajaCartas[0];
            barajaCartas.RemoveAt(0);
            montonCartas.Add(carta);
            return carta;
        }

        public void CartasDisponibles()
        {
            Console.WriteLine("Hay "+ barajaCartas.Count + " cartas en la baraja.");
        }

        public List<Carta> DarCartas(int numCartas)
        {
            if (numCartas > barajaCartas.Count)
            {
                Console.WriteLine("No hay suficientes cartas en la baraja");
                return null;
            }

            List<Carta> cartas = new List<Carta>();
            for (int i = 0; i < numCartas; i++)
            {
                Carta carta = SiguienteCarta();
                if (carta != null)
                {
                    cartas.Add(carta);
                }
                else
                {
                    break;
                }
            }
            return cartas;
        }

        public void CartasMonton()
        {
            if (montonCartas.Count == 0)
            {
                Console.WriteLine("No ha salido ninguna carta aún");
            }
            else
            {
                Console.WriteLine("Cartas que han salido:");
                foreach (Carta carta in montonCartas)
                {
                    Console.WriteLine(carta.Numero +" de "+ carta.Palo);
                }
            }
        }

        public void MostrarBaraja()
        {
            Console.WriteLine("Cartas en la baraja:");
            foreach (Carta carta in barajaCartas)
            {
                Console.WriteLine(carta.Numero + " de " + carta.Palo);
            }
        }
        public void MostrarBarajaJugador(List<Carta> cartasJugador)
        {
            Console.WriteLine("El jugador tiene "+cartasJugador.Count+" cartas:");
            foreach (var carta in cartasJugador)
            {
                Console.WriteLine(carta.Numero + " de " + carta.Palo);

            }
        }
    }

    class BarajaApp
    {
        static void Main()
        {
            //Instanciamos la clase Baraja
            Baraja barajaPruebas = new Baraja();
            //Barajamos las cartas
            barajaPruebas.Barajar();
            //Mostramos las cartas del Mazo Inicialmente
            barajaPruebas.CartasDisponibles();
            //Inicializamos las listas que contendran las cartas de los jugadores.
            List <Carta> cartasJugador;
            List <Carta> cartasJugador2;

            //Los jugadores cojen cartas
            Console.WriteLine("EL JUGADOR 1 COJE 5 CARTAS");
            cartasJugador=barajaPruebas.DarCartas(5);
            barajaPruebas.MostrarBarajaJugador(cartasJugador);
            barajaPruebas.SiguienteCarta();
            Console.WriteLine("EL JUGADOR 2 COJE 2 CARTAS");
            cartasJugador2 = barajaPruebas.DarCartas(5);
            barajaPruebas.MostrarBarajaJugador(cartasJugador);
            //Pasan cartas
            barajaPruebas.SiguienteCarta(); 
            barajaPruebas.SiguienteCarta(); 
            barajaPruebas.SiguienteCarta();
            //Mostramos las cartas restantes del mazo
            barajaPruebas.CartasDisponibles();
            //Mostramos las cratas del monton
            barajaPruebas.CartasMonton();

        }
    }
}
