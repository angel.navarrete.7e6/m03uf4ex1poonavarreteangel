﻿using System;

namespace AhorrosCuentaBancaria
{
    class Cuenta
    {
        public string titular { get; set; }
        public double cantidad { get; set; }

        public Cuenta(string titular)
        {
            this.titular = titular;
        }
        public Cuenta(string titular, double cantidad)
        {
            this.titular = titular;
            this.cantidad = cantidad;
        }
        public string toString()
        {
            return "Titular: " + this.titular + ", cantidad: " + this.cantidad +"€";
        }
        public void ingresar(double cantidad)
        {
            if (cantidad > 0)
            {
                this.cantidad += cantidad;
            }
            else
            {
                Console.WriteLine("Error Cantidad Negativa");
            }
        }
        public void retirar(double cantidad)
        {
            if (cantidad > this.cantidad)
            {
                this.cantidad = 0;
            }
            else
            {
                this.cantidad -= cantidad;
            }
        }
    }

    class CuentaApp
    {
        static void Main(string[] args)
        {
            Cuenta cuenta1 = new Cuenta("Angeles");
            Cuenta cuenta2 = new Cuenta("Fernando", 300);


            //Ingresa dinero en las cuentas
            cuenta1.ingresar(300);
            cuenta2.ingresar(400);


            //Retiramos dinero en las cuentas
            cuenta1.retirar(500);
            cuenta2.retirar(100);


            //Muestro la informacion de las cuentas
            Console.WriteLine(cuenta1.toString()); // 0 euros
            Console.WriteLine(cuenta2.toString()); // 600 euros
        }

    }
}
