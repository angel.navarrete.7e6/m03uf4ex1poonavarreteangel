﻿using System;

namespace AgendaContactos
{
    public class Contacto
    {
        public string Nombre { get; set; }
        public int Telefono { get; set; }

        public Contacto(int numeroTelefono, string nombreContacto)
        {
            this.Nombre = nombreContacto;
            this.Telefono = numeroTelefono;
        }
        public override bool Equals(Object obj)
        {
            if ((obj == null) || GetType() != obj.GetType())
            {
                return false;
            }
            Contacto contacto = (Contacto)obj;
            if (contacto.Nombre == this.Nombre)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
    public class Agenda
    {
        public Contacto[] Contactos { get; set; }
    
        public Agenda()
        {
            this.Contactos =  new Contacto[10];
        }
        public Agenda(int cantidad)
        {
            this.Contactos = new Contacto[cantidad];
        }

        public bool AniadirContacto(Contacto contactoNuevo)
        {
            if (ExisteContacto(contactoNuevo)==true) Console.WriteLine("El contacto ya existe");
            else if (AgendaLlena()==true) Console.WriteLine("La Agenda esta llena");
            else
            {
                for (int i = 0; i < Contactos.Length; i++)
                {
                    if (Contactos[i] == null)
                    {
                        Contactos[i] = contactoNuevo;
                        Console.WriteLine("Contacto "+ contactoNuevo.Nombre + " añadido");
                        return true;
                    }
                }
            }
            return false;
        }
        public bool ExisteContacto(Contacto contacto)
        {
            foreach (var contactoAgenda in Contactos)
            {
                if (contactoAgenda != null && contacto.Equals(contactoAgenda) == true)
                {
                    return true;
                }
            }
            return false;
            
        }
        public void ListarContactos()
        {
            Console.WriteLine("\nAgenda: ");
            foreach (var contacto in Contactos)
            {
                if (contacto != null)
                {
                    Console.WriteLine("- " + contacto.Nombre + ", " + contacto.Telefono);
                }
            }
        }
        public void BuscaContacto(string nombre) {
            foreach (var contacto in Contactos)
            {
                if (contacto != null)
                {
                    if (contacto.Nombre.ToUpper() == nombre.ToUpper())
                    {
                        Console.WriteLine("El Telefono de " + contacto.Nombre + " es: " + contacto.Telefono);
                    }
                }
                
            }
        }
        public void EliminarContacto(Contacto contacto) {
            for (int i = 0; i < Contactos.Length; i++)
            {
                if (Contactos[i] != null && Contactos[i].Equals(contacto))
                {
                    Contactos[i] = null;
                    Console.WriteLine("Contacto "+contacto.Nombre + " eliminados");
                }
            }
        
        }
        public bool AgendaLlena() {
            foreach (var contacto in Contactos)
               {
                if (contacto == null) return false;
            }
            return true;
        }
        public void HuecosLibres() {
            int huecos = 0;
            foreach (var contacto in Contactos)
            {
                if (contacto == null)
                {
                    huecos++;
                }
            }
            Console.WriteLine("Hay "+huecos+ " huecos libres.");
        }

    }

    class AgendaContactosApp
    {
        static void Main(string[] args)
        {
            Agenda agenda = new Agenda();
            agenda.HuecosLibres();
            Contacto manolo = new Contacto(123456748, "Manolo");
            agenda.AniadirContacto(manolo);
            Contacto manolo2 = new Contacto(87654321, "Manolo");
            agenda.AniadirContacto(manolo2);
            Contacto angustia = new Contacto(159753264, "Angustia");
            agenda.AniadirContacto(angustia);
            Contacto federico = new Contacto(246875135, "Federico");
            agenda.AniadirContacto(federico);
            Contacto paco = new Contacto(649654964, "Paco");
            agenda.AniadirContacto(paco);
            Console.WriteLine("BUSCAMOS EL NUMERO DE ANGUSTIA: ");
            agenda.BuscaContacto("Angustia");
            agenda.ListarContactos();
            Console.WriteLine("ELIMINAMOS A PACO");
            agenda.EliminarContacto(paco);
            agenda.ListarContactos();
        }
    }
}
