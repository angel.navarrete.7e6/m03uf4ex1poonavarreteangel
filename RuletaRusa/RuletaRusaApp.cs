﻿using System;

namespace RuletaRusa
{
    public class Revolver
    {
        public int PosicionTambor { get; set; }
        public int PosicionBala { get; set; }
        public Revolver()
        {
            Random rnd = new Random();
            this.PosicionTambor = rnd.Next(1, 7);
            this.PosicionBala = rnd.Next(1, 7);
        }
        public bool Disparar()
        {
            if (PosicionBala == PosicionTambor)
            {
                return true;
            }
            else
            {
                SiguienteBala();
                return false;
            }
        }
        public void SiguienteBala()
        {
            if (PosicionTambor == 6)
            {
                PosicionTambor = 1; 
            }
            else
            {
                PosicionTambor++;
            }
        }
        public override string ToString()
        {
            return "Posicion de la Bala: " + this.PosicionBala + "Posicion del tambor para disparar: " + this.PosicionTambor;
        }
    }
    public class Jugador
    {
        public int Id_jugador { get; set; }
        public string NombreJugador { get; set; }
        public bool Vivo { get; set; }
        public Jugador(int id) {
            this.Id_jugador = id;
            this.NombreJugador = "Jugador " + this.Id_jugador;
            this.Vivo = true;
        }

        public void Disparar(Revolver revolver)
        {
            Console.WriteLine("El "+this.NombreJugador +" se apunta con el revolver, se morirà? Hagan sus apuestas.");

            if (revolver.Disparar() == true)
            {
                Console.WriteLine(this.NombreJugador + " eliminado, ni un balazo aguanta.");
                this.Vivo = false;
            }
            else
            {
                Console.WriteLine(this.NombreJugador + " de momento sigue con vida, pero no por mucho tiempo.");
            }
        }
    }
    public class Juego
    {
        public Jugador[] Jugadores { get; set; }
        public Revolver RevolverJuego { get; set; }

        public Juego(int numJugadores) {
            if (numJugadores > 1 && numJugadores < 6)
            {
                this.Jugadores = new Jugador[numJugadores];
                for (int i = 0; i < numJugadores; i++)
                {
                    this.Jugadores[i] = new Jugador(i + 1);
                }
                this.RevolverJuego = new Revolver();
            }
            else
            {
                this.Jugadores = new Jugador[numJugadores];
                for (int i = 0; i < numJugadores; i++)
                {
                    this.Jugadores[i] = new Jugador(i + 1);
                }
                this.RevolverJuego = new Revolver();
            }
        }

        public bool FinJuego()
        {
            foreach (var jugador in Jugadores)
            {
                if (jugador.Vivo == false)
                {
                    Console.WriteLine("FIN DEL JUEGO");
                    return true;
                }
            }
            return false;
        }
        public void Ronda()
        {
            for (int i = 0; i < Jugadores.Length; i++)
            {
                Jugadores[i].Disparar(RevolverJuego);
                if (FinJuego() == true)
                {
                    break;
                }
            }      
        }
    }

    class RuletaRusaApp
    {
        static void Main()
        {
            Console.WriteLine("EMPIEZA LA RULETA RUSA, QUIEN MORIRA, QUIEN SOBREVIVIRÁ LO VEREMOS: ");
            Juego app = new Juego(6);
            app.Ronda();
        }
    }
}
