﻿using System;

namespace DatosPersonas
{
    class Persona
    {
        public string Nombre { get; set; }
        public int Edad { get; set; }
        public string DNI { get; }
        public char Sexo { get; set; }
        public int Peso { get; set; }
        public int Altura { get; set; }

        public Persona()
        {
            this.Nombre = "";
            this.Edad = 0;
            this.DNI = this.GenerarDNI();
            this.Sexo = 'H';
        }
        public Persona(string nombre, int edad, char sexo)
        {
            this.Nombre = nombre;
            this.Edad = edad;
            this.DNI = this.GenerarDNI();
            this.Sexo = sexo;
        }
        public Persona(string nombre, int edad, string DNI,char sexo)
        {
            this.Nombre = nombre;
            this.Edad = edad;
            this.DNI = DNI;
            this.Sexo = sexo;
        }

        public Persona(string nombre, int edad, char sexo, int peso, int altura)
        {
            this.Nombre = nombre;
            this.Edad = edad;
            this.DNI = this.GenerarDNI();
            this.Sexo = sexo;
            this.Peso = peso;
            this.Altura = altura;
        }

        public double CalculaIMC() {
            double valorIMC = (this.Peso /Math.Pow(this.Altura,2))*10000;
            if (valorIMC >= 20)
            {
                return valorIMC;
            }
            else
            {
                return -1;
            }
        }
        public bool EsMayorDeEdad()
        {
            if (this.Edad >= 18)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool ComprobarSexo() {
            if (this.Sexo == 'H' || this.Sexo == 'M')
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private string GenerarDNI()
        {
            string numerosDNI = "";
            Random rnd = new Random();
            for (int i = 0; i < 8; i++)
            {
                numerosDNI += rnd.Next(1, 10);
            }
            int numeroLetra = Convert.ToInt32(numerosDNI) % 23;
            string[] letras = { "T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E" };
            string dni = numerosDNI + letras[numeroLetra];
            return dni;
        }
        public override string ToString()
        {
            return "Persona con nombre "+this.Nombre + " con edad "+ this.Edad +" i DNI "+this.DNI + " de sexo "+ this.Sexo + " tiene una altura de "+this.Altura + " i un peso de "+ this.Peso;
        }
    }
    class PersonaApp
    {
        static void Main(string[] args)
        {
            //Pedir datos al Usuario
            Console.Write("Introduce tu nombre: ");
            string nombre = Console.ReadLine();
            Console.Write("Introduce tu sexo: ");
            char sexo = Convert.ToChar(Console.ReadLine());
            Console.Write("Introduce tu edad: ");
            int edad = Convert.ToInt32(Console.ReadLine());
            Console.Write("Introduce tu peso: ");
            int peso = Convert.ToInt32(Console.ReadLine());
            Console.Write("Introduce tu altura: ");
            int altura = Convert.ToInt32(Console.ReadLine());

            //Creacion de Personas
            Persona persona1 = new Persona(nombre, edad, sexo, peso, altura);
            Persona persona2 = new Persona(nombre, edad, sexo);
            persona2.Peso = 90;
            persona2.Altura = 200;
            Persona persona3 = new Persona();
            persona3.Nombre = "Arturo";
            persona3.Edad = 23;
            persona3.Sexo = 'H';
            persona3.Peso = 30;
            persona3.Altura = 150;

            //Calcular IMC de cada Persona.
            Console.WriteLine("\n- IMC: ");
            PersonaApp app = new PersonaApp();
            app.MostrarIMC(persona1);
            app.MostrarIMC(persona2);
            app.MostrarIMC(persona3);

            //Mostrar si es mayor de edad
            Console.WriteLine("\n- Mayor Edad: ");
            app.MostrarMayorEdad(persona1);
            app.MostrarMayorEdad(persona2);
            app.MostrarMayorEdad(persona3);

            //Mostrar la informacion de las personas
            Console.WriteLine("\n- Informacion de las Personas: ");
            Console.WriteLine(persona1.ToString());
            Console.WriteLine(persona2.ToString());
            Console.WriteLine(persona3.ToString());
        }
        void MostrarIMC(Persona persona)
        {
            if (persona.CalculaIMC() == -1)
            {
                Console.WriteLine("La persona " + persona.Nombre + "tiene peso inferior al normal");
            }
            else if (persona.CalculaIMC() >= 20 && persona.CalculaIMC()<= 24.5)
            {
                Console.WriteLine("La persona "+ persona.Nombre+ " tiene peso normal");
            }
            else if (persona.CalculaIMC() >= 25 || persona.CalculaIMC() <= 29.9)
            {
                Console.WriteLine("La persona "+ persona.Nombre +" tiene sobrepeso");
            }
            else if (persona.CalculaIMC() >= 30)
            {
                Console.WriteLine("La persona " + persona.Nombre + "tiene obesidad");
            }
        }

        void MostrarMayorEdad(Persona persona)
        {
            if (persona.EsMayorDeEdad() == true)
            {
                Console.WriteLine("La persona"+persona.Nombre+" es mayor de edad");
            }
            else
            {
                Console.WriteLine("La persona" + persona.Nombre + " es menor de edad");
            }
        }
    }
}
