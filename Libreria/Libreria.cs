﻿using System;

namespace Libreria
{
    class Libro
    {
        public string ISBN { get; set; }
        public string Titulo { get; set; }
        public string Autor { get; set; }
        public int NumPaginas { get; set; }
        public Libro() { }
        public Libro(string ISBN,string Titulo,string Autor, int NumPaginas)
        {
            this.ISBN = ISBN;
            this.Titulo = Titulo;
            this.Autor = Autor;
            this.NumPaginas = NumPaginas;
        }
        public override string ToString()
        {
            return "El libro con nombre " + this.Titulo + " i ISBN " + this.ISBN + " por el autor " + this.Autor + " tiene " + this.NumPaginas + " paginas.";
        }
    }
    class Libreria
    {
        static void CompararPaginas(Libro libro1, Libro libro2)
        {
            if (libro1.NumPaginas > libro2.NumPaginas)
            {
                Console.WriteLine("El libro con más paginas es: "+libro1.Titulo);
            }
            else
            {
                Console.WriteLine("El libro con más paginas es: " + libro2.Titulo);
            }
        }
        static void Main(string[] args)
        {
            Libro fnaf1 = new Libro("9788416867356", "FIVE NIGHTS AT FREDDY S. LOS OJOS DE PLATA","Scott Cawthon",400);
            Console.WriteLine(fnaf1.ToString());
            Libro eldenRing = new Libro("3869931175", "Elden Ring: Los Libros Del Saber, Volumen 1", "Bandai Namco", 512);
            Console.WriteLine(eldenRing.ToString());
            Libreria.CompararPaginas(fnaf1, eldenRing);

        }
    }
}
